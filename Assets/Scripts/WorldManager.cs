﻿using UnityEngine;

namespace Assets.Scripts
{
    public class WorldManager : MonoBehaviour
    {
        // Singleton instance
        public static WorldManager Instance { get; private set; }

        // Properties
        public Chunk Root;
        public Address Focus = new Address("0");
        public float ChunkScale = float.MaxValue;
        public uint ChunkScaleLevel;

        private void Awake()
        {
            // Singleton safety. Destroy if an instance already exists
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }


            // Instantiate a world chunk if one isn't provided.
            if (Root == null)
            {
                Root = gameObject.AddComponent<Chunk>();
                Root.transform.parent = gameObject.transform;
            }
        }
    }
}