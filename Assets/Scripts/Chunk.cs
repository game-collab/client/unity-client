﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Chunk : MonoBehaviour
    {
        public ChunkMaterial ChunkMaterial;
        public Address Address;

        // TODO: Create a custom property drawer for the UI for children so the order makes sense
        public Chunk[] Children = new Chunk[8];
        public GameObject RenderedCube;
        public Vector3 Position = Vector3.zero;

        [SerializeField] private bool _isVisible;

        private WorldManager _worldManager;

        private Vector3 PositionOffset => _worldManager.Focus.ToVector3Offset();

        public bool IsVisible
        {
            get { return _isVisible && ChunkMaterial.IsVisible; }
            set
            {
                if (ChunkMaterial.IsVisible)
                {
                    _isVisible = value;
                    RenderedCube.GetComponent<Renderer>().enabled = IsVisible;
                }
            }
        }

        public void Awake()
        {
            if (RenderedCube == null)
            {
                RenderedCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                RenderedCube.name = "Chunk";
                RenderedCube.transform.parent = gameObject.transform;
            }

            RenderedCube.transform.position = Position;
            _worldManager = WorldManager.Instance;

            RenderedCube.transform.position = PositionOffset;
            RenderedCube.transform.localScale = Vector3.one;
        }
    }
}