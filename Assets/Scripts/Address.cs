﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class Address
    {
        [SerializeField] private string _addressString;

        public Address(string addressString)
        {
            AddressString = addressString;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public string AddressString
        {
            get { return _addressString; }
            set
            {
                foreach (var c in value)
                {
                    if (!"01234567".ToCharArray().Contains(c))
                    {
                        throw new ArgumentException("Address must consist of only numbers ranging from 0 to 7");
                    }
                }

                _addressString = value;
            }
        }

        public int Scale => _addressString.Length;

        public override string ToString()
        {
            return _addressString;
        }

        public Vector3 ToVector3Offset()
        {
            return new Vector3(0f, 0f, 0f);
        }

        public IEnumerable<int> ToIntEnumerable()
        {
            return from c in _addressString select Convert.ToInt32(c);
        }

        public IEnumerable<int[]> ToCoordEnumerable()
        {
            return from c in _addressString select ToIntArray(c);
        }

        private static int[] ToIntArray(char c)
        {
            var integer = Convert.ToInt32(c.ToString(), 8);
            if (0 < integer || integer > 7)
            {
                throw new ArgumentException("Address must consist of only numbers ranging from 0 to 7");
            }

            var binary = Convert.ToString(integer, 2);
            return new[]
            {
                Convert.ToInt32(binary[0].ToString(), 2),
                Convert.ToInt32(binary[1].ToString(), 2),
                Convert.ToInt32(binary[2].ToString(), 2)
            };
        }
    }
}