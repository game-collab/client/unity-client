﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{


        public delegate void OnCameraMoved();

        public OnCameraMoved OnCameraMovedCallback;

        public void Thing()
        {
                OnCameraMovedCallback?.Invoke();
        }
}
