﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        // Singleton instance
        // ReSharper disable once MemberCanBePrivate.Global
        public static GameManager Instance { get; private set; }

        // Properties
        public WorldManager WorldManager;

        private void Awake()
        {
            // Singleton safety. Destroy if an instance already exists
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

            WorldManager = WorldManager.Instance != null
                ? WorldManager.Instance
                : gameObject.AddComponent<WorldManager>();
            WorldManager.transform.parent = gameObject.transform;
        }
    }
}