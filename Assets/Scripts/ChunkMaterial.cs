﻿using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "New Material", menuName = "Chunk Material")]
    public class ChunkMaterial : ScriptableObject
    {
        public bool IsVisible;
    }
}